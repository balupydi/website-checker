Website Checker
==================


*A Website Checker Automation written in two Python modules*


Description:
------------

This automation is for pulling stats on a specific website and push them on to kafka bus which is carried out in Module one "websiteChecker".
Also, module two "dbWriter" has the kafka consume which reads the messages constantly and write them to Postgres Database.


Setup:
------
 - websiteChecker has to be configured as a cron job to run at regular interval.
 - dbWriter can be run as a service on the linux system.