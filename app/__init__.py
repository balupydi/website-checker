"""
Author: Balakrishna Pydi
Description: This init module is to import logging and setup up basic configuration to it
"""
import os
import logging

LOG_PATH = "/opt/website-checker/logs/"
if not os.path.exists(LOG_PATH):
    os.makedirs(LOG_PATH)
logging.basicConfig(filename='{}debug.log'.format(LOG_PATH),
                    format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)