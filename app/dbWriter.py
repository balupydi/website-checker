"""
Author: Balakrishna Pydi
Description: This module perform kafka message read operation and writes data onto DB.
"""

try:
    import logging
    from kafka import KafkaConsumer, TopicPartition
    import psycopg2
    import ast
    import base64

except ImportError as err:
    print("Import Error: {}".format(err))

class dbWriter:
    """dbWriter class consists all methods to perform writes operation on to DB"""

    logger = logging.getLogger(__name__)
    connection, cursor, tname = None, None, None

    def __init__(self, tname):
        """__init__ constructor used to open connection with database and create able if not exist"""
        try:
            self.tname = tname
            self.connection = psycopg2.connect(user="bala",
                                          password="bala",
                                          host="0.0.0.0",
                                          port="5432",
                                          database="website_stats")
            self.cursor = self.connection.cursor()
            self.logger.info("{} connection created...".format(self.__class__))
            stable = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_name='{}';".format(self.tname)
            self.cursor.execute(stable)
            record = self.cursor.fetchone()
            if record is None:
                ctable = '''CREATE TABLE {} (ID SERIAL PRIMARY KEY, StatusCode INT, ResponseTimeInSeconds REAL, PatternMatch TEXT);'''.format(self.tname)
                self.cursor.execute(ctable)
                self.connection.commit()
        except Exception as exe:
            self.logger.error("{} connection creation Failed: {}".format(self.__class__, exe))

    def __del__(self):
        """__del__ destructor is used to close database connection"""
        self.cursor.close()
        self.connection.close()
        self.logger.info("{} connection closed...".format(self.__class__))

    def writetoPostgresDB(self, q):
        """Update kafka message into the DB"""
        try:
            if "PatternMatch" not in list(q.keys()):
                itable = "INSERT INTO {} (statusCode, responseTimeInSeconds) VALUES (%s,%s) ".format(
                    self.tname)
                insertv = (q['StatusCode'], q['ResponseTimeInSeconds'])
            else:
                itable = "INSERT INTO {} (statusCode, responseTimeInSeconds, patternMatch) VALUES (%s,%s,%s) ".format(
                    self.tname)
                insertv = q['StatusCode'], q['ResponseTimeInSeconds'], base64.b64encode((str(q['PatternMatch']).encode("utf-8")))
            self.cursor.execute(itable, insertv)
            self.connection.commit()
            print("Written to DB: {}".format(q))
            return True
        except Exception as exe:
            self.logger.error("writetoPostgresDB operation failed on {}: {}".format(self.__class__, exe))
            return False


    def readMessagefromKafka(self, istest=False):
        """Kafka consumer reads the latest message and writes to db"""
        try:
            consumer = KafkaConsumer('sample', bootstrap_servers='localhost:9092', auto_offset_reset='latest', enable_auto_commit=False, group_id='my-group' )
            for msg in consumer:
                dmsg = ast.literal_eval(msg.value.decode("UTF-8"))
                status = self.writetoPostgresDB(dmsg)
                if istest:
                    return status
            self.logger.info("Message produced on to Kafka!")
            return status
        except Exception as exe:
            self.logger.error("readMessagefromKafka operation failed on {}: {}".format(self.__class__, exe))
            return False

if __name__ == "__main__":
    obj = dbWriter("statmessages")
    obj.readMessagefromKafka()