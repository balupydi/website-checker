"""
Author: Balakrishna Pydi
Description: This module perform website checking and writes message on to kafka.
"""

try:
    import logging
    import requests
    from kafka import KafkaConsumer, KafkaProducer
    from confluent_kafka.admin import AdminClient, NewTopic
    import confluent_kafka.admin


except ImportError as err:
    print("Import Error: {}".format(err))

class websiteChecker:
    """websiteChecker class consists all methods to perform website checking and writes message on to kafka"""

    logger = logging.getLogger(__name__)

    def sendMessagetoKafka(self, topic, message):
        """Writes gathers website info as message onto kafka. Create topic is not available """
        try:
            consumer = KafkaConsumer(bootstrap_servers='localhost:9092')
            if topic not in consumer.topics():
                admin_client = confluent_kafka.admin.AdminClient({"bootstrap.servers": "localhost:9092"})
                new_topic = confluent_kafka.admin.NewTopic(topic, 1, 1)
                print(admin_client)
                print(new_topic)
                admin_client.create_topics([new_topic,])
            producer = KafkaProducer(bootstrap_servers='localhost:9092')
            producer.send('sample', bytes(str(message), 'utf-8'))
            self.logger.info("Sent message onto Kafka!")
            print("Message Sent!")
            return True
        except Exception as exe:
            self.logger.error("sendMessagetoKafka operation failed on {}: {}".format(self.__class__, exe))
            return False

    def checkWebsite(self, url, pattern=None):
        """Performs the checks on the website and gather the required metrics"""
        try:
            response = requests.request("GET", url, headers={}, data = {})
            kafka_message = {"StatusCode": response.status_code, "ResponseTimeInSeconds": response.elapsed.total_seconds()}
            if pattern is not None:
                tdic = {index: x for index, x in enumerate(response.text.split('\n'), start=1)}
                lfound = [{"Message": "{} Match Found!".format(pattern)}]
                for key, val in tdic.items():
                    if pattern in val:
                        lfound.append({key: val})
                if len(lfound) == 1:
                   lfound[0]["Message"] = "{} Match Not Found!".format(pattern)
                kafka_message["PatternMatch".format(pattern)] = lfound
            self.logger.info("checkWebsite Success: {}".format(kafka_message))
            return self.sendMessagetoKafka("sample", kafka_message)
        except Exception as exe:
            self.logger.error("checkWebsite operation failed on {}: {}".format(self.__class__, exe))
            return False


if __name__ == "__main__":
    obj = websiteChecker()
    obj.checkWebsite("https://aiven.io/")
    obj.checkWebsite("https://aiven.io/", "images.ctfassets.net")
    obj.checkWebsite("https://aiven.io/", "BalakrishnaPydi")