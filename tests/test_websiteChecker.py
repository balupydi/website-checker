from unittest import TestCase
from app.websiteChecker import websiteChecker

class test_websiteChecker(TestCase):

    def test_checkWebsite(self):
        obj = websiteChecker()
        status = obj.checkWebsite("https://aiven.io/")
        self.assertTrue(status)

    def test_sendMessagetoKafka(self):
        obj = websiteChecker()
        message = {'StatusCode': 200, 'ResponseTimeInSeconds': 0.117422, 'PatternMatch': [{'Message': 'BalakrishnaPydi Match Not Found!'}]}
        status = obj.sendMessagetoKafka("test",message)
        self.assertTrue(status)