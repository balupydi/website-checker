from unittest import TestCase
from app.dbWriter import dbWriter

class test_dbWriter(TestCase):

    def test_readMessagefromKafka(self):
        obj = dbWriter("statmessages")
        status = obj.readMessagefromKafka(True)
        self.assertTrue(status)

    def test_writetoPostgresDB(self):
        obj = dbWriter("statmessages")
        message = {'StatusCode': 200, 'ResponseTimeInSeconds': 0.117422, 'PatternMatch': [{'Message': 'BalakrishnaPydi Match Not Found!'}]}
        status = obj.writetoPostgresDB(message)
        self.assertTrue(status)